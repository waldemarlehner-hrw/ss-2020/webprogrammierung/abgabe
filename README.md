# Project: Singlepage Application: CoinChecker by Waldemar and Julian

Bei unserer Singlepage application handelt es sich um eine Page, auf der man sich
Informationen über Cryptowährungen holen kann, sowie diese grafisch anzeigen zu lassen, mehrere Währungen zu vergleichen oder gar Preisveränderungen der letzten
Zeit zu veranschaulichen. Diese sind jeweils in Euro, US-Dollar (USD) oder Rubel
(RUB) dynamisch verfügbar.

## URL zur SPA

Die Anwendung kann [hier](https://waldemarlehner-hrw.gitlab.io/ss-2020/webprogrammierung/abgabe/) aufgerufen werden.
Das Git-Repository kann [hier](https://gitlab.com/waldemarlehner-hrw/ss-2020/webprogrammierung/abgabe/) nach dem Abgabedatum betrachtet werden.

## Auflistung fremder Codebestandteile oder gar Seiten, die zur Information dienten

Zunächst ist vorzumerken, dass wir unseren CoinChecker der Cryptowährungen mit Hilfe der API bzw. Informationen von Coingecko erstellt haben. [Coingecko API](https://www.coingecko.com/de/api#explore-api)

1. [Aufsetzen der GitLab’s continous integration (CI) mit Vue](https://knasmueller.net/vue-js-on-gitlab-pages)
2. [Erstellung des Chartes von ApexCharts](https://apexcharts.com/vue-chart-demos/line-charts/basic/)
3. [Verwenden von Zeitstempel mit ApexCharts](https://apexcharts.com/javascript-chart-demos/area-charts/datetime-x-axis/)
4. [Das verwendete Frontend-Framework MaterializeCSS](https://materializecss.com/). Komponenten wie Slider(Carousel), Modal sowie Buttons von MaterializeCSS wurden verwendet.
5. [Wie funktioniert die Vue Watch Funktion? - Um allgemein Änderungen der Properies herauszufinden](https://stackoverflow.com/questions/44584292/how-to-listen-for-props-changes)
6. [Wie bekommt man den Zeitstempel von vor 7 Tagen](https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before)

## Beschreibung und Begründung unserer Architektur unseres Codes

Unsere Singlepage application besteht aus 6 Komponenten.
Die Architektur ist so zu verstehen:

### App.vue

Dies ist die Hauptkomponente. Sie ist für das Aktualisieren des Coin-Arrays
zuständig. Die darunter liegenden Komponenten reagieren reaktiv auf die
Änderungen des Coin-Arrays bzw. der ausgewählten Währung.
Wenn die Liste neue Daten holt wird `isListReady` auf false gesetzt.
Dieser Boolean wird an das darunter liegende Element übergeben.
Dieses zeigt dann einen Loader bis die erste Antwort zurückkommt.

### Ticker.vue

Der Ticker ist die oberste Komponente. Hier werden die 12 Coins mit dem höchsten
Volumen angezeigt. Die Komponente zeigt für jede Währung den aktuellen Wert, die
Änderung in den letzten 24 Tagen sowie das Minimum und Maximum in
den letzen 24 Stunden.

Wenn den Nutzer die Währung ändert wird eine neue API-Request mit
der neuen Währung erstellt.

### Searchbar.vue

Diese Komponente beinhaltet die Suchleiste sowie die Währungsauswahl.
Die Suchleiste wird Mithilfe MaterializeCSSs Autofill-Funktionalität realisiert.

Beim Laden der Seite wird ein API-Request erstellt, welche alle Coins mit ID,
Namen und Symbol auflistet. Diese Daten werden in einem Array abgespeichert
(siehe `mappings.data` in der Komponente). Es wurde zudem ein Dictionary mithilfe
eines Objektes erstellt, mit welchem man die über den Namen bzw. das Symbol die
weiteren Daten für die Cryptowährung bekommen kann. (siehe `mappings.nameToIndex` und
`mappings.symbolToIndex`) Für das erstellen der Mappings ist die Funktion
`generateAutofillMappings` verantworlich.
Das Input-Feld gibt als Autofill-Option alle Cryptowährung-Namen bis auf die bereits ausgewählten an.
Dadurch kann eie Cryptowährung nicht 2 mal in die Liste aufgenommen werden.
Für das generieren der Autofill-Daten ist `generateAutofillMaterialize`
verantwortlich. Wenn `selectedCoins`, das Prop, welches alle aktuell
gewählten Coins repräsentiert, modifiziert wird, werden die Autofill-Daten neu
berechnet.

Das Dropdown mit den Währungen aktualisiert im Eltern-Element (`App.vue`) den
data-Wert `currency` und ruft dort `updateData` auf.
Dazu mehr im Abschnitt App.vue

### CoinList.vue

Diese Komponente beinhaltet die Liste aller ausgewählten Coins sowie deren Modals
(*Ein Modal ist quasi ein Pop-Up. Wir verwenden Modals um für ein Coin mehr
Informationen darzustellen*).
CoinList hat das Prop `list`, welches alle benötigten Daten übergibt.
Die Aktualisierung von Coins und Währung geschieht in App.vue,
die Liste muss sich also darum nicht kümmern und handelt reaktiv auf die Änderung
des Props. Für jeden Eintrag in `list` wird ein `CoinListEntry` und
`CoinListModal` erzeugt.

### CoinListEntry.vue

Diese Komponente beinhaltet einen Listeneintrag.
Die Daten werden über das Prop `entry` übergeben. Dort sind die Relevanten Werte,
nämlich *Name, Bild-URL, jetziger Wert, Änderung 24h in ausgewählter Währung und
in %*  abgespeichert.
Der genaue Aufbau kann in `definitions/ListEntry.d.ts` angeschaut werden.
Diese Typescript-Definitionsdatei wird nirgendwo im Code verwendet.
Wir verwenden sie ausschließlich zum "Nachschlagen"
der Datenstruktur des Coin-Objektes.

Auf der rechten Seite eines Eintrages kann über den Button "More Info" das
dazu-gehörige Modal geöffnet werden. Dazu mehr unter *CoinListModal.vue*.
Über den Button "Remove" wird das Element aus `selectedCoins` entfernt.
Die CoinList wird dadurch neu gerendert, und das Element ist nicht mehr vorhanden.

### CoinListModal.vue

Diese Komponente beinhaltet das Modal für eine Cryptowährung. Dieses bekommt wie ein
CoinListEntry über das Prop `entry` die benötigten Daten.
Die id des Modals hängt vom Symbol des entry ab. Dadurch kann ein CoinListEntry
das dazugehörige Modal problemlos auffinden.
(siehe dazu: `openModal` im Method-Bereich des Vue-Objektes.)

Durch das Klicken außerhalb des aktiven Bereiches bzw. auf das Kreuz in der
oberen rechten Ecke wird das Modal geschlossen. Das Schließen erfolgt ähnlich wie
das öffnen. Die Komponente kennt das Symbol der Cryptowährung, da es über das Prop übergeben
wird und kann sich somit selbst referenzieren und schließen.

### Chart.vue

Diese Komponente ist die letzte auf der Seite. Hier werden von allen ausgewählten
Coins die Werte aus den letzen 7 Tagen mithilfe von ApexCharts dargestellt.
Es sollte angemerkt werden dass die API die Werte aus den letzen 7 Tagen nur in
USD übergibt. Deswegen ist die Währung für den Graphen (USD) auf diesen vorgeschrieben.
Die API gibt den Wert-Verlauf des Coins in einem 1-D Array zurück.
Dieser muss für das Darstellen in einen 2-D Array konvertiert werden. Dafür wird `definitions/apexChartsDataFunction.js` verwendet.
Genaueres ist in dieser Datei im Kommentar aufzufinden.

Es sollte angemerkt werden dass nicht alle Cryptowährungen Historiedaten haben.
Wenn die Liste den ausgegrauten Text *No Price Change Data /* anzeigt ist dies
der Fall. Es werden nur Coins mit Historiedaten an das Chart übergeben.
Dafür sorgt in App.vue bei der Übergabe des Properties eine Filter-Funktion,
welche nur die Coins aus `coinData` übergibt, welche Historie-Daten haben.

## Project setup

1. Das heruntergeladene Archiv entpacken
2. Visual Studio starten --> File --> Open Folder anklicken und den durch die Entpackung des Archives entstandenen Ordner `abgabe` auswählen
3. `npm install` im Hauptverzeichnis aufrufen. Dies Installiert alle benötigten Abhängigkeiten.
4. `npm run serve` im Hauptverzeichnis aufrufen. Dies startet den Entwicklungsserver. Die Anwendung kann dann mit den in der Konsole angezeigten Pfaden aufgerufen werden.
