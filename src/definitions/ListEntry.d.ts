///
/// Wird im Code nicht verwendet, da das übergeben von Interfaces über Reguläres JS nicht machbar ist. Wird jedoch zum "Nachschlagen" verwendet.
///
export type ListEntry = {
	name: String,
	id: String,
	symbol: String,
	sparkline_7d?: Number[],
	description: String,
	priceChange: PriceChange,
	imageFilePath: String,
}


/**
 * Maps Delta Changes to a currency. Currency is stored in the ListEntry
 */
export type PriceChange = {
	current?: Number,
	change24h?: Number,
	change24hPercentage?: Number,
	change7dPercentage?: Number,
	change14dPercentage ?: Number,
	change30dPercentage ?: Number,
}

export type Currency = {
	id: String,
	name: String,
	symbol: String
}

