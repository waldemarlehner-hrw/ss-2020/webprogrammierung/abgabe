
export default {
	getColorClassesByValue,
	returnPlusIfPositive
}

/**
* @param value {number}
* @returns {String}
*/
function getColorClassesByValue (value) {
	if (typeof value !== 'number') {
		return 'grey-text text-lighten-1'
	}
	if (value === 0) {
		return ''
	}
	if (value > 0) {
		return 'green-text text-darken-3'
	}
	if (value < 0) {
		return 'red-text text-darken-3'
	}
}

function returnPlusIfPositive (num) {
	return (num > 0) ? '+' : ''
}
